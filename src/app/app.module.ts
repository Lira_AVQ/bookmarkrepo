import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { A11yModule } from '@angular/cdk/a11y';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';

import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { initialState, reducers, effects } from './store/state/app.state';

import { BookmarkComponent } from './components/bookmark/bookmark.component';
import { DialogComponent } from './components/dialog/dialog.component';
import { MessageComponent } from './components/dialog/dialog.component';
import { CommonTableComponent } from './components/common-table/common-table.component';
import { AddBookmarkComponent } from './components/add-bookmark/add-bookmark.component';
import { UpdateBookmarkComponent } from './components/update-bookmark/update-bookmark.component';

import { WebsiteService } from './services/website.service';
import { NotificationService } from './services/notification.service';

@NgModule({
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        A11yModule,
	    MatButtonModule,
	    MatButtonToggleModule,
	    MatDialogModule,
	    MatIconModule,
	    MatInputModule,
	    MatMenuModule,
	    MatSidenavModule,
        MatSnackBarModule,
	    MatTableModule,
        MatToolbarModule,
	    MatTooltipModule,
        FormsModule,
        EffectsModule.forRoot(effects),
        StoreModule.forRoot(reducers, {initialState}),
        StoreDevtoolsModule.instrument( {maxAge: 30} ),
    ],
    declarations: [
        AppComponent,
        BookmarkComponent,
        DialogComponent,
        MessageComponent,
        CommonTableComponent,
		AddBookmarkComponent,
		UpdateBookmarkComponent
    ],
    providers: [
	    WebsiteService, NotificationService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }