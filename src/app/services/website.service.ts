import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Websites } from '../store/state/website.state';

@Injectable({providedIn: 'root'})
export class WebsiteService {

    private dummyWebsites: Websites = {
        websites: [
            {id:1, name: 'Tech Mahindra', url: 'https://www.techmahindra.com/', group: 'Work'},
            {id:2, name: 'Google', url: 'https://www.google.com/', group: 'Leisure'},
            {id:3, name: 'Gmail', url: 'https://mail.google.com/', group: 'Personal'},
            {id:4, name: 'Avaloq', url: 'https://www.avaloq.com/', group: 'Work'},
            {id:5, name: 'Yahoo', url: 'https://ph.yahoo.com/', group: 'Personal'}
        ]
    };

    getWebsites(): Observable<Websites>{
        return new Observable(observer => {
            observer.next(this.dummyWebsites);
            observer.complete();
        });
    }

}
