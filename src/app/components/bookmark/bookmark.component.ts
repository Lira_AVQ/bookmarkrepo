import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { AddBookmarkComponent } from '../add-bookmark/add-bookmark.component';
import { UpdateBookmarkComponent } from '../update-bookmark/update-bookmark.component';
import { AppState, selectWebsites, selectGroup, selectWebsitesByGroup } from '../../store/state/app.state'; 
import { Website } from '../../store/state/website.state';
import { LoadWebsiteInit, DeleteWebsite } from '../../store/actions/website.actions';
import { NotificationService } from '../../services/notification.service';

@Component({
    selector: 'bookmark',
    styleUrls: ['bookmark.component.scss'],
    templateUrl: 'bookmark.component.html',
})

export class BookmarkComponent {
    bookmarkTableConfig;
    addDialogConfig;
    activeFolder;
    groups$: Observable<string[]>;
    groups: string[];
    websites$: Observable<Website[]>;
    websites: Website[];
    selectedGroup: string;

    constructor(private store: Store<AppState>) {
    }

	ngOnInit() {
	    this.addDialogConfig = {
			type: 'template',
			attribute: 'mat-icon-button',
			fontIconColor: 'accent',			
			fontIcon:'bookmark_add',
			tooltip:'Add Bookmark',			
			template: AddBookmarkComponent
		}

		this.loadData();
		
		this.bookmarkTableConfig = {
			data: this.websites,
			displayedColumns: ['name', 'url', 'action'],
			columns: [{
				header: "Name",
				field: "name"
			},{
				header: "URL",
				field: "url"
			}],
			editDialogConfig: {
				type: 'template',
				attribute: 'mat-menu-item',
				fontIconColor: 'accent',
				fontIcon:'edit',
				tooltip:'Edit Bookmark',
				text:'Edit',
				template: UpdateBookmarkComponent
			},
			deleteDialogConfig: {
				type: 'message',
				attribute: 'mat-menu-item',
				fontIconColor: 'warn',
				fontIcon:'close',
				tooltip:'Delete Bookmark',
				text:'Delete',
				messageConfig: {
				    message: 'Are you sure you want to delete this bookmark?',
                    primaryButtonEnabled: true,
                    primaryButtonLabel: 'Yes',
                    secondaryButtonEnabled: true,
                    secondaryButtonLabel: 'No',
                    primaryButtonCallback: function(store: Store<AppState>, notificationService: NotificationService, data: any) {
	                    store.dispatch(new DeleteWebsite(data));
					    notificationService.showNotification({
						    duration: 2000,
						    vPos: 'top',
						    hPos: 'center',
						    message: 'Bookmark was deleted successfully.'
					    });
                    }
				}
			}
		}
	}

    private loadData() {
	    this.groups$ = this.store.pipe(select(selectGroup));
        this.groups$.subscribe(response => {
		    this.groups=response;
        });

        this.loadBookmark('All');
        this.activeFolder='All';
        this.loadWebsites();
    }

    private loadWebsites() {
	    this.store.dispatch(new LoadWebsiteInit(null));
    }
	
	isActive(group: string) {
	    return group === this.activeFolder;
	}
	
    loadBookmark(group : string) {
	    this.selectedGroup = group;
        if(group === 'All') {
	        this.websites$ = this.store.pipe(select(selectWebsites));
        } else {
	        this.websites$ = this.store.pipe(select(selectWebsitesByGroup(group)));
        }

        this.websites$.subscribe(response => {
		    this.websites=response;

			if(this.bookmarkTableConfig) {
				this.bookmarkTableConfig.data=this.websites;
			}
        });

        this.activeFolder = group;
    }

}
