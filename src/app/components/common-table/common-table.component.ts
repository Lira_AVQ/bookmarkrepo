import { Component, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/state/app.state';
import { NotificationService } from '../../services/notification.service';

interface CommonTableConfig {
	columns: any[]
	displayedColumns: string[]
    tooltip: any
    data: any
    editDialogConfig: any
    deleteDialogConfig: any
    deleteFunc: Function
}

@Component({
	selector: 'common-table',
    styleUrls: ['common-table.component.scss'],
    templateUrl: 'common-table.component.html',
})

export class CommonTableComponent {
	
    constructor(private store: Store<AppState>, private notificationService: NotificationService) {
    }

    @Input() config : CommonTableConfig;

	delete(data: any) {
		if(data){
			this.config.deleteFunc(data, this.store, this.notificationService);
		}		
	}
	
}