import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RouterTestingModule } from '@angular/router/testing';
import { AddBookmarkComponent } from './add-bookmark.component';
import { NotificationService } from '../../services/notification.service';
import { Store } from '@ngrx/store';
import { FormsModule } from "@angular/forms";

describe('AddBookmarkComponent', () => {
	let component: AddBookmarkComponent;
	let fixture: ComponentFixture<AddBookmarkComponent>;
	let notificationServiceSpy: jasmine.SpyObj<NotificationService>;
	let storeSpy: jasmine.SpyObj<Store>;

	beforeEach(() => {
		const spy = jasmine.createSpyObj('NotificationService', ['showNotification']);
		const storeMock = jasmine.createSpyObj('Store', ['website']);
		storeMock.website(({
			name: 'Tech Mahindra',
			url: 'https://www.techmahindra.com/',
			group: 'Work'
		})
		);

		TestBed.configureTestingModule({
			imports: [
				FormsModule, RouterTestingModule
			],
			providers: [
				{ provide: NotificationService, useValue: spy },
				{ provide: Store, useValue: storeMock }
			]
		});

		notificationServiceSpy = TestBed.inject(NotificationService) as jasmine.SpyObj<NotificationService>;
		storeSpy = TestBed.inject(Store) as jasmine.SpyObj<Store>;
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(AddBookmarkComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should add bookmark', () => {
		expect(component).toBeTruthy();
	});
	
});
