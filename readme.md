# Bookmark Web Application

A bookmark application that makes use of Angular 9+, NGRX for managing state and Angular Material for user interface. 

## SETUP
npm version: 6.13.7

node version: 13.9.0

port used: 1818

####  To install node_modules
on project directory > npm install

####  To start application
on project directory >  npm start

####  To test
add-bookmark is the component that has test functionality.

on project directory > ng test

## FUNCTIONALITY
The bookmark application is used to Add, Edit, Delete and View bookmarks by groups.

####  Add
![Add] (screenshots/add-bookmark.png)

####  Update (Edit)
![Update] (screenshots/update-bookmark.png)

####  Delete
![Delete] (screenshots/delete-bookmark.png)

####  View
![View] (screenshots/view-bookmark.png)